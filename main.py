import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
from graphviz import Digraph

class Automata:

    def __init__(self):
        self.graph = [['' for _ in range(300)] for _ in range(300)]
        #self.graph = np.full((20, 20), '', dtype=np.dtype('i'))
        self.used_node = 0
        self.nodes = []
        self.final_node = 0
        self.counter_debug = 0

    def node_successors(self, node):
        """
            Successors for one node [ (node, link) ]
        """
        nodes = []

        for col in range(len(self.graph[node])):
            if self.graph[node][col] != '':
                nodes.append((col, self.graph[node][col]))

        return nodes

    def node_predecessors(self, node):
        """
            Predeccessors for one node [ (node, link) ]
        """
        nodes = []

        for row in range(len(self.graph)):
            if self.graph[row][node] != '':
                nodes.append((row, self.graph[row][node]))

        return nodes

    def node_predecessors_with_same_link(self, node):
        counter = {}
        for node_to_test, link in self.node_predecessors(node):
            if link in counter:
                counter[link][1] += 1
                counter[link][0].append(node_to_test)
            else:
                counter[link] = [[node_to_test], 1]

        for link, nodes in counter.items():
            if nodes[1] > 1:
                return nodes[0]
        
        return []
    
    def node_successors_with_same_link(self, node):
        counter = {}
        for node_to_test, link in self.node_successors(node):
            if link in counter:
                counter[link][1] += 1
                counter[link][0].append(node_to_test)
            else:
                counter[link] = [[node_to_test], 1]

        for link, nodes in counter.items():
            if nodes[1] > 1:
                return nodes[0]
        
        return []
    
    def is_non_zero_reversible(self):
        for node in range(self.used_node):
            if self.node_predecessors_with_same_link(node) != [] or self.node_successors_with_same_link(node) != []:
                return True
                
        return False

    def fusion(self, nodes):
        """
            do fusion of all nodes in a list nodes and return the node kept
        """
        node_to_keep = nodes.pop(0)

        for node in nodes:
            for pred, link in self.node_predecessors(node):
                # connect the predecessor to node_to_kepp
                self.graph[pred][node_to_keep] = self.graph[pred][node]

                # disconnect predecessor from previous node
                self.graph[pred][node] = ''

            for succ, link in self.node_successors(node):
                # connect the successor to node_to_kepp
                self.graph[node_to_keep][succ] = self.graph[node][succ]

                # disconnect successor from previous node
                self.graph[node][succ] = ''
        
        return node_to_keep

    def node_successor(self, actual_node, c):
        """
            return the next node with transition c from actual_node
            or return -1 if the node doesn't exist
        """
        for col in range(len(self.graph[actual_node])):
            if self.graph[actual_node][col] == c:
                return col
        return -1

    def display_graph(self):
        network = nx.DiGraph()
        network.add_node(0)

        for col in range(len(self.graph[0])):
            for row in range(len(self.graph)):
                if(self.graph[row][col]):
                    network.add_node(col)

        for source in range(len(self.graph)):
            for destination in range(len(self.graph[source])):
                if(self.graph[source][destination] != ''):
                    network.add_edge(source, destination, weight=self.graph[source][destination])

        nx.draw(network, with_labels=True)
        plt.show()

    def display_graph_graphviz(self):
        dot = Digraph(comment='Automata')

        # node
        for node in range(self.used_node):
            if self.node_predecessors(node) != [] and self.node_successors(node) != []:
                dot.node(str(node), str(node))

        # edge
        for node in range(self.used_node):
            successors = self.node_successors(node)
            for s, link in successors:
                dot.edge(str(node), str(s), label=str(link))

        dot.render(f"test-output/{self.counter_debug}automata.gv", view=True)
        self.counter_debug += 1

    def add_word(self, word):
        actual_node = 0

        for c in word:
            potential_node = self.node_successor(actual_node, c)
            if potential_node == -1:
                # create a new node for this character
                self.used_node += 1
                self.graph[actual_node][self.used_node] = c
                actual_node = self.used_node
            else:
                actual_node = potential_node

        return actual_node # return the terminal node for the added word
    
    def optimize(self):
        while self.is_non_zero_reversible():
            for node in range(self.used_node):
                if self.node_successors_with_same_link(node) != []:
                    self.fusion(self.node_successors_with_same_link(node))
                if self.node_predecessors_with_same_link(node) != []:
                    self.fusion(self.node_predecessors_with_same_link(node))

    def learn_words(self, words):
        final_node = []

        for word in words:
            final_node.append(self.add_word(word))

        self.fusion(final_node)
        
        self.optimize()


S1 = [
    [],
    [0, 0],
    [1, 1],
    [0, 0, 0, 0],
    [0, 1, 0, 1],
    [0, 1, 1, 0],
    [1, 0, 1, 0]
]

S2 = [
    [2, 0],
    [1, 1],
    [3, 0, 0, 0],
    [0, 3, 0, 1],
    [1, 2, 1, 0],
    [1, 2, 1, 0],
    [2, 2, 0, 1, 3],
    [1, 2, 5, 2, 0],
    [4, 5, 4, 2, 2]
]

automata = Automata()
automata.learn_words(S2)
automata.display_graph_graphviz()

